from pandas import *
import pandas as pd


def calculate_points(in_file):
    # Read the Excel
    me_dict_object = excel_to_dict(in_file, "Match Extract")
    ar_dict_object = excel_to_dict(in_file, "Auction Result")
    lb_dict_object = excel_to_dict(in_file, "Leader Board")

    column_names = [k for k in me_dict_object.keys()]
    row_count = len(me_dict_object[column_names[0]])
    extract_details = []
    for i in range(row_count):
        extract_detail = {}
        for column in column_names:
            extract_detail[column] = me_dict_object[column][i]
        extract_details.append(extract_detail)
        # print(extract_detail)

    for player in extract_details:
        points_earned = 2
        # Calculate Batsman points
        points_earned += player["Runs Scored"]
        points_earned += (player["Four"] * 1)
        points_earned += (player["Six"] * 2)
        if player["Runs Scored"] >= 30:
            points_earned += 5
        if player["Runs Scored"] >= 50:
            points_earned += 10
        if player["Runs Scored"] >= 100:
            points_earned += 10

        # Calculate Bowler points
        points_earned += (player["Wickets"] * 20)
        if player["Wickets"] >= 3:
            points_earned += 10
        if player["Wickets"] >= 5:
            points_earned += 10
        points_earned += (player["Maiden"] * 20)
        points_earned += (player["LBW"] * 5)
        points_earned += (player["Bowled"] * 5)
        points_earned += (player["Hit Wicket"] * 5)

        # Calculate other points
        points_earned += (player["Catches"] * 5)
        points_earned += (player["Runout"] * 10)
        points_earned += (player["Stumping"] * 10)
        points_earned += (player["Man of the Match"] * 30)

        player["Points Earned"] = points_earned
        # print(player)

    a_column_names = [k for k in ar_dict_object.keys()]
    a_row_count = len(ar_dict_object[a_column_names[0]])
    auction_details = []
    for i in range(a_row_count):
        auction_detail = {}
        for column in a_column_names:
            auction_detail[column] = ar_dict_object[column][i]
        auction_detail["Total Points"] = 0
        auction_details.append(auction_detail)
        # print(auction_detail)

    for auction_detail in auction_details:
        player_name = auction_detail["Player Name"]
        total_points = auction_detail["Total Points"]
        for player in extract_details:
            if str(player["Player Name"]).lower() == str(player_name).lower():
                total_points += player["Points Earned"]
        auction_detail["Total Points"] = total_points
        # print(auction_detail)

    # Write to Html
    df = pd.DataFrame(auction_details)
    dfg = df.groupby(['Player Name', 'Team Owner', 'Total Points']).sum()
    dfg.to_html('PlayerPoint.html')

    lb_column_names = [k for k in lb_dict_object.keys()]
    lb_row_count = len(lb_dict_object[lb_column_names[0]])
    leader_board_details = []
    for i in range(lb_row_count):
        leader_board_detail = {}
        for column in lb_column_names:
            leader_board_detail[column] = lb_dict_object[column][i]
        leader_board_details.append(leader_board_detail)
        # print(leader_board_detail)

    for leader_board_detail in leader_board_details:
        owner_name = leader_board_detail["Team Owner"]
        total_points = leader_board_detail["Total Points"]
        for owner in auction_details:
            if str(owner["Team Owner"]).lower() == str(owner_name).lower():
                total_points += int(owner["Total Points"])
        total_points += int(leader_board_detail["Previous Points"])
        leader_board_detail["Total Points"] = total_points
        # print(leader_board_detail)

    leader_board_details = sorted(leader_board_details, key=lambda d: d['Total Points'], reverse=True)
    rank = 1
    for leader_board_detail in leader_board_details:
        leader_board_detail["Rank"] = rank
        print(leader_board_detail)
        rank += 1

    leader_board_details = sorted(leader_board_details, key=lambda d: d['Rank'])

    import csv

    keys = leader_board_details[0].keys()

    with open('Rank.csv', 'w', newline='') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(leader_board_details)

    # Write to Html
    df = pd.DataFrame(leader_board_details)
    dfg = df.groupby(['Rank', 'Team Owner', 'Total Points', 'Previous Points']).sum()
    dfg.to_html('Rank.html')

    pass


def excel_to_dict(in_file, sheet):
    # convert into dataframe
    xls = ExcelFile(in_file)
    data = xls.parse(sheet)
    # print(data.to_dict('list'))
    # df = pd.read_excel(in_file, sheet_name=sheet)
    # convert into dictionary
    # print(df.to_dict())
    return data.to_dict('list')


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    input_file = "UplivingFantacy.xlsx"
    calculate_points(input_file)
